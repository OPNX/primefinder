package com.emanueldasilva.primefinder;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    public static final String FILE_NAME = "largest_prime.txt";
    private Button button;
    private EditText primeView;
    private long currentPrime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.button);

        currentPrime = 2;

        primeView = findViewById(R.id.prime_view);

        loadFileOntoTextView();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findNextPrime(v);
                setPrimeView(currentPrime);
                saveOntoFile();
            }
        });

    }

    private void findNextPrime(View view){

        boolean isPrime = false;
        long candidate = ++currentPrime;

            while(!(isPrime)){
                try {

                    if(isOdd(candidate) && isPrime(candidate)){
                        currentPrime = Integer.parseInt(candidate + "");
                        Toast.makeText(MainActivity.this, "New prime number found!", Toast.LENGTH_SHORT).show();
                        isPrime = true;
                    } else {
                        ++candidate;
                    }

                    Thread.sleep(125);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try{
                Thread.sleep(125);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

    }

    private void setPrimeView(long prime){
        git piush primeView.setText("");
        primeView.setText(prime + "");
    }

    private boolean isOdd(long candidate){
        if(candidate % 2 ==1){
            return true;
        } else {
            return false;
        }
    }

    private boolean isPrime(long candidate) {
        long sqrt = (long)Math.sqrt(candidate);
        for(long i = 3; i <= sqrt; i += 2)
            if(candidate % i == 0) return false;
        return true;
    }

    private void loadFileOntoTextView() {

        try {
            FileInputStream fileInputStream = openFileInput(FILE_NAME);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new DataInputStream(fileInputStream)));

            String s = (bufferedReader.readLine());

            currentPrime = Long.parseLong(s);

            setPrimeView(currentPrime);

            Toast.makeText(this, "Last saved prime number loaded", Toast.LENGTH_SHORT).show();

            fileInputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveOntoFile() {
        try {

            setPrimeView(currentPrime);

            FileOutputStream fileOutputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);

            String primeString = currentPrime + "";

            fileOutputStream.write(primeString.getBytes());

            fileOutputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}